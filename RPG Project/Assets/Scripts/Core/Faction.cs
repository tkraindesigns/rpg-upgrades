﻿using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    
    public enum Factions
    {
        Player,
        Guards,
        Beasts,
        Bandits,
        Peasants
    }
    
    [DisallowMultipleComponent]
    public class Faction: MonoBehaviour
    {
        [SerializeField] Factions team;

        private HashSet<Factions> Player = new HashSet<Factions>() { Factions.Beasts, Factions.Bandits };
        private HashSet<Factions> Guards = new HashSet<Factions>() { Factions.Beasts, Factions.Bandits };
        private HashSet<Factions> Beasts = new HashSet<Factions>() { Factions.Player, Factions.Guards, Factions.Bandits, Factions.Peasants };
        private HashSet<Factions> Bandits = new HashSet<Factions>() { Factions.Player, Factions.Guards, Factions.Beasts, Factions.Peasants };
        private HashSet<Factions> Peasants = new HashSet<Factions>() { Factions.Bandits, Factions.Beasts };
        
        Factions charmedTeam;
        float charmedTimeRemaining=0;

        private Dictionary<Factions, HashSet<Factions>> HashSets = new Dictionary<Factions, HashSet<Factions>>();
        
        public Factions Team => charmedTimeRemaining>0? charmedTeam:team;

        void Awake()
        {
            HashSets.Add(Factions.Player, Player);
            HashSets.Add(Factions.Bandits, Bandits);
            HashSets.Add(Factions.Beasts, Beasts);
            HashSets.Add(Factions.Guards, Guards);
            HashSets.Add(Factions.Peasants, Peasants);
        }
        
        public void  Charm(Factions newTeam, float charmedDuration)
        {
            if(Team == newTeam) charmedTimeRemaining+=charmedDuration; 
            else
            {
                charmedTeam = newTeam;
                charmedTimeRemaining = charmedDuration;
            }
        }

        public bool CanAttack(Factions otherFaction)
        {
            return HashSets[Team].Contains(otherFaction);
        }
        
        void Update()
        {
            if(charmedTimeRemaining>0) charmedTimeRemaining-=Time.deltaTime;
        }
    }
}