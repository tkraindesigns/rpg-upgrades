using UnityEngine;

namespace RPG.Control.States
{
    public class PatrolState : AIState
    {
        private const string Currentwaypointindex = "CurrentWaypointIndex";

        public PatrolState(AIStateMachine machine) : base(machine)
        {
        }

        private Vector3 currentWaypoint;
        private int currentWaypointIndex = 0;
        private float dwellTime = 0;
    
        public override void Enter()
        {
            Debug.Log($"{stateMachine.name} entering Patrol State");
            currentWaypoint = stateMachine.transform.position;
            if (stateMachine.PatrolPath != null)
            {
                currentWaypointIndex = stateMachine.PatrolPath.GetNearestWaypoint(stateMachine.transform.position);
                currentWaypoint = stateMachine.PatrolPath.GetWaypoint(currentWaypointIndex);
                Debug.Log($"{stateMachine.name} beginning Patrol on waypoint {currentWaypointIndex} -> {currentWaypoint}");
                
            }
            stateMachine.Mover.StartMoveAction(currentWaypoint, 1f);
            
        }

        public override void Tick(float deltaTime)
        {
            if (InRangeOfPlayer())
            {
                stateMachine.BeginState(new AttackState(stateMachine));
                return;
            }
            if (stateMachine.PatrolPath != null)
            {
                if (dwellTime > 0)
                {
                    dwellTime -= deltaTime;
                    if (dwellTime <= 0)
                    {
                        stateMachine.Mover.StartMoveAction(currentWaypoint, 1.0f);
                    }
                    return;
                }
                if (Vector3.Distance(stateMachine.transform.position, currentWaypoint) <
                    stateMachine.WaypointAcceptanceRadius)
                {
                    Debug.Log($"{stateMachine.name} has reached waypoint {currentWaypointIndex}, beginning dwell.");
                    stateMachine.Mover.Cancel();
                    dwellTime = stateMachine.WaypointDwellTime * Random.Range(.8f, 1.2f);
                    currentWaypointIndex = stateMachine.PatrolPath.GetNextIndex(currentWaypointIndex);
                    currentWaypoint = stateMachine.PatrolPath.GetWaypoint(currentWaypointIndex);
                }
            }
        }

        public override void Aggrevate()
        {
            stateMachine.UpdateLastKnownPlayerPosition();
            stateMachine.BeginState(new InvestigateState(stateMachine));
        }

        public override void ReactToHit(float damage)
        {
            stateMachine.AggrevateOthers();
            stateMachine.BeginState(new AttackState(stateMachine));
        }

        public override void Exit()
        {
        
        }
    }
}