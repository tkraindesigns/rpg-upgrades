﻿using UnityEngine;

namespace RPG.Control.States
{
    public class InvestigateState : AIState
    {
        private float dwellTime = 0;
        
        public InvestigateState(AIStateMachine machine) : base(machine)
        {
        }

        public override void Enter()
        {
            stateMachine.Mover.StartMoveAction(stateMachine.LastKnownPlayerPosition, 1.0f);
        }

        public override void Tick(float deltaTime)
        {
            if (InRangeOfPlayer())
            {
                stateMachine.BeginState(new AttackState(stateMachine));
                return;
            }

            if (dwellTime > 0)
            {
                dwellTime += deltaTime;
                if (dwellTime <= 0)
                {
                    stateMachine.BeginState(new PatrolState(stateMachine));
                }
                return;
            }

            if (Vector3.Distance(stateMachine.transform.position, stateMachine.LastKnownPlayerPosition) <
                stateMachine.WaypointAcceptanceRadius)
            {
                stateMachine.Mover.Cancel();
                dwellTime = stateMachine.WaypointDwellTime;
            }
        }

        public override void ReactToHit(float damage)
        {
            stateMachine.BeginState(new AttackState(stateMachine));
            
        }

        public override void Aggrevate()
        {
            stateMachine.UpdateLastKnownPlayerPosition();
            Enter();
        }

        public override void Exit()
        {
            
        }
    }
}