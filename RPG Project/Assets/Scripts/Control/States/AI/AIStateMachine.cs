using System.Collections.Generic;
using RPG.Attributes;
using RPG.Combat;
using RPG.Movement;
using RPG.Stats;
using UnityEngine;

namespace RPG.Control.States
{
    [RequireComponent(typeof(Health))]
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(BaseStats))]
    [RequireComponent(typeof(Fighter))]
    [RequireComponent(typeof(CombatTarget))]

    public class AIStateMachine : MonoBehaviour
    {
   


        [field: SerializeField] public Mover Mover { get; private set; }
        [field: SerializeField] public Fighter Fighter { get; private set; }
        [field: SerializeField] public BaseStats BaseStats { get; private set; }
        [field: SerializeField] public Health Health { get; private set; }

        [field: SerializeField] public PatrolPath PatrolPath { get; private set; }
        [field: SerializeField] public float WaypointAcceptanceRadius { get; private set; }
        [field: SerializeField] public float WaypointDwellTime { get; private set; }
        [field: SerializeField] public float AttackRange { get; private set; }
        [field: SerializeField] public float FieldOfVision { get; private set; }
        [field: SerializeField] public bool CanFear { get; private set; }
        
        public Vector3 LastKnownPlayerPosition;

        private GameObject player;
        public GameObject Player => player;
    
        private AIState currentState = null;


        private void OnValidate()
        {
            if (WaypointAcceptanceRadius <= 0) WaypointAcceptanceRadius = 2.0f;
            if (WaypointDwellTime <= 0) WaypointDwellTime = 5.0f;
            if (AttackRange <= 0) AttackRange = 8.0f;
            if (FieldOfVision <= 0) FieldOfVision = 30f;
            if (Mover == null) Mover = GetComponent<Mover>();
            if (Fighter == null) Fighter = GetComponent<Fighter>();
            if (BaseStats == null) BaseStats = GetComponent<BaseStats>();
            if (Health == null) Health = GetComponent<Health>();
        
        }

        private void Start()
        {
            player = GameObject.FindWithTag("Player");
            BeginState(new PatrolState(this));
            this.Health.takeDamage.AddListener(ReactToHit);
        }

        private void Update()
        {
            if (currentState != null)
            {
                currentState.Tick(Time.deltaTime);
            }
        }

        public void BeginState(AIState newState)
        {
            if (currentState != null)
            {
                currentState.Exit();
            }

            currentState = newState;
            currentState.Enter();
        }

        public void UpdateLastKnownPlayerPosition()
        {
            LastKnownPlayerPosition = Player.transform.position;
        }

        public void Aggrevate()
        {
            if (currentState!=null)
            {
                currentState.Aggrevate();
            }
        }

        void ReactToHit(float damage)
        {
            if (currentState != null)
            {
                currentState.ReactToHit(damage);
            }
        }
        
        public void AggrevateOthers()
        {
            foreach (var aiStateMachine in FindObjectsOfType<AIStateMachine>())
            {
                if (aiStateMachine == this) continue;
                if (Vector3.Distance(aiStateMachine.transform.position, transform.position) < AttackRange)
                {
                    aiStateMachine.Aggrevate();
                }
            }
        }
        
    }
}