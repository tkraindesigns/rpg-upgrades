using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control.States
{
    public abstract class  AIState : State
    {
        protected AIStateMachine stateMachine;
    
        public AIState(AIStateMachine machine)
        {
            stateMachine = machine;
        }

        protected float DistanceToPlayer()
        {
            return Vector3.Distance(stateMachine.transform.position, stateMachine.Player.transform.position);
        }

        protected bool InRangeOfPlayer()
        {
            return DistanceToPlayer() < stateMachine.AttackRange;
        }

        public virtual void Aggrevate()
        {
            
        }

        public virtual void ReactToHit(float damage)
        {
            
        }
        
        protected bool FacingPlayer()
        {
            Vector3 targetDir = stateMachine.Player.transform.position - stateMachine.transform.position;
            return Vector3.Angle(targetDir, stateMachine.transform.forward) < stateMachine.FieldOfVision;
        }
        
    }
}