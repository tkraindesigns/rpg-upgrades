﻿namespace RPG.Control.States
{
    public class AttackState : AIState
    {
        public AttackState(AIStateMachine machine) : base(machine)
        {
        }

        public override void Enter()
        {
            stateMachine.Fighter.Attack(stateMachine.Player);
        }

        public override void Tick(float deltaTime)
        {
            stateMachine.UpdateLastKnownPlayerPosition();
            if (!InRangeOfPlayer())
            {
                stateMachine.BeginState(new InvestigateState(stateMachine));
            }
        }

        public override void ReactToHit(float damage)
        {
            if (stateMachine.CanFear) return;
            if (damage > stateMachine.Health.GetHealthPoints())
            {
                
            } 
        }

        public override void Exit()
        {
            
        }
    }
}