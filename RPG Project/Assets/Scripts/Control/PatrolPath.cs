﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour
    {
        const float waypointGizmoRadius = 0.3f;

        private void OnDrawGizmos() {
            for (int i = 0; i < transform.childCount; i++)
            {
                int j = GetNextIndex(i);
                Gizmos.DrawSphere(GetWaypoint(i), waypointGizmoRadius);
                Gizmos.DrawLine(GetWaypoint(i), GetWaypoint(j));
            }
        }

        public int GetNextIndex(int i)
        {
            if (i + 1 == transform.childCount)
            {
                return 0;
            }
            return i + 1;
        }

        public Vector3 GetWaypoint(int i)
        {
            return transform.GetChild(i).position;
        }

        
        
        public int GetNearestWaypoint(Vector3 location)
        {
            float closestDistance = Vector3.Distance(location, transform.GetChild(0).position);
            int result = 0;
            for (int i = 0; i < transform.childCount; i++)
            {
                float dist = Vector3.Distance(location, transform.GetChild(i).position);
                if (dist < closestDistance)
                {
                    closestDistance = dist;
                    result = i;
                }
            }
            return result;
        }
    }
}