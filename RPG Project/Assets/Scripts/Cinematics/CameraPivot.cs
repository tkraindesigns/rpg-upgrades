﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG.Cinematics
{
    
    public class CameraPivot : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed;
        private Vector2 lastScreenPosition;
        private bool isDragging = false;
        private Transform player;
        private void Awake()
        {
            player = GameObject.FindWithTag("Player").transform;
        }

        void Update()
        {
            transform.position = player.position;
            if (Input.GetMouseButtonDown(1))
            {
                if (EventSystem.current.IsPointerOverGameObject()) return;
                isDragging = true;
                lastScreenPosition = Input.mousePosition;
                return;
            }
            if (Input.GetMouseButtonUp(1))
            {
                isDragging = false;
                return;
            }
            if (!isDragging) return;
            float delta = Input.mousePosition.x - lastScreenPosition.x;
            lastScreenPosition = Input.mousePosition;
            if (Mathf.Abs(delta) < 2) return; //filter out mouse noise
            float direction = Mathf.Sign(delta);
            transform.Rotate(Vector3.up, direction * rotationSpeed * Time.deltaTime);
        }
    }
}