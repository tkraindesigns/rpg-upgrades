﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.UI;
using UnityEngine;
using RPG.Dialogue;
using TMPro;
using UnityEngine.UI;

namespace RPG.UI
{
    public class DialogueUI : MonoBehaviour
    {
        PlayerConversant playerConversant;
        [SerializeField] TextMeshProUGUI AIText;
        [SerializeField] Button nextButton;
        [SerializeField] GameObject AIResponse;
        [SerializeField] Transform choiceRoot;
        [SerializeField] GameObject choicePrefab;
        [SerializeField] Button quitButton;
        [SerializeField] TextMeshProUGUI conversantName;

        // Start is called before the first frame update
        void Start()
        {
            playerConversant = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerConversant>();
            playerConversant.onConversationUpdated += UpdateUI;
            nextButton.onClick.AddListener(() => playerConversant.Next());
            quitButton.onClick.AddListener(() => playerConversant.Quit());
            ShowHideUI.OnModalActive += playerConversant.Quit;
            UpdateUI();
        }

        private void OnDestroy()
        {
            ShowHideUI.OnModalActive -= playerConversant.Quit;
        }

        void UpdateUI()
        {
            gameObject.SetActive(playerConversant.IsActive());
            if (!playerConversant.IsActive())
            {
                return;
            }
            conversantName.text = playerConversant.GetCurrentConversantName();
            AIResponse.SetActive(!playerConversant.IsChoosing());
            choiceRoot.gameObject.SetActive(playerConversant.IsChoosing());
            if (playerConversant.IsChoosing())
            {
                BuildChoiceList();
            }
            else
            {
                AIText.text = playerConversant.GetText();
                nextButton.gameObject.SetActive(playerConversant.HasNext());
            }
        }

        private List<GameObject> existingChoices;

        private void BuildChoiceList()
        {
            // if (existingChoices == null)
            // {
            //     existingChoices = new List<GameObject>();
            //     foreach (Transform item in transform)
            //     {
            //         existingChoices.Add(item.gameObject);
            //     }
            // }
            foreach (Transform item in choiceRoot)
            {
                Destroy(item.gameObject);
            }
            // foreach (GameObject existingChoice in existingChoices)
            // {
            //     existingChoice.SetActive(false);
            // }
            //int i = 0;
            foreach (DialogueNode choice in playerConversant.GetChoices())
            {
               GameObject choiceInstance = Instantiate(choicePrefab, choiceRoot);
               //GameObject choiceInstance = existingChoices[i++];
               choiceInstance.SetActive(true);
                var textComp = choiceInstance.GetComponentInChildren<TextMeshProUGUI>();
                textComp.text = choice.GetText();
                Button button = choiceInstance.GetComponentInChildren<Button>();
                button.onClick.AddListener(() => 
                {
                    playerConversant.SelectChoice(choice);
                });
            }
        }
    }
}