﻿using RPG.Quests;
using UnityEditor;
using UnityEngine;

public class QuestListUI : MonoBehaviour
{
    [SerializeField] QuestItemUI questPrefab;
    QuestList questList;
    
    // Start is called before the first frame update
    void Start()
    {
        questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
        questList.onUpdate += Redraw;
        Redraw();
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (questPrefab != null)
        {
            var status = PrefabUtility.GetPrefabInstanceStatus(questPrefab.gameObject);
            if (status != PrefabInstanceStatus.NotAPrefab)
            {
                Debug.Log($"{questPrefab} is not a prefab");
                questPrefab = null;
            }
        }
    }
#endif
    private void Redraw()
    {
        foreach (Transform item in transform)
        {
            Destroy(item.gameObject);
        }
        Debug.Assert(questList, "questList does not exist");
        foreach (QuestStatus status in questList.GetStatuses())
        {
            Debug.Assert(status!=null, "status in list does not exist");
            Debug.Assert(questPrefab, "questPrefab does not exist");
            QuestItemUI uiInstance = Instantiate<QuestItemUI>(questPrefab, transform);
            Debug.Assert(uiInstance, $"uiInstance is null.  Does {questPrefab} have a QuestItemUI component?");
            uiInstance.Setup(status);
        }
    }
}
