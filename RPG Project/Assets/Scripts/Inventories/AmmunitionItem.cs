﻿using GameDevTV.Inventories;
using UnityEngine;

namespace RPG.Inventories
{
    [CreateAssetMenu(fileName = "Ammunition", menuName = "RPG/Ammunition/New Ammunition", order = 0)]
    public class AmmunitionItem : InventoryItem
    {
        
    }
}