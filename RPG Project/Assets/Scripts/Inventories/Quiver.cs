﻿
using GameDevTV.Inventories;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace RPG.Inventories
{
    public class Quiver : MonoBehaviour, IJsonSaveable
    {
        public event System.Action quiverUpdated;
        
        private AmmunitionItem currentAmmunition=null;
        private int amount = 0;
        

        public bool HasSufficientAmmunition(AmmunitionItem requiredAmmunition, int requiredAmount = 1)
        {
            return currentAmmunition != null && (requiredAmmunition == currentAmmunition) && amount >= requiredAmount;
        }

        public void SpendAmmo(int amountToSpend)
        {
            amount -= amountToSpend;
            if (amount <= 0)
            {
                currentAmmunition =null;
                amount = 0;
            }
            quiverUpdated?.Invoke();
        }

        public AmmunitionItem GetItem() => currentAmmunition;
        public int GetAmount() => amount;

        public void RemoveAmmunition(int removeAmount = -1)
        {
            if (removeAmount < 0) removeAmount = amount;
            amount -= removeAmount;
            if (amount <= 0)
            {
                currentAmmunition = null;
                amount = 0;
            }
            quiverUpdated?.Invoke();
        }

        public void AddAmmunition(AmmunitionItem item, int number)
        {
            
                currentAmmunition = item;
                amount = number;
            
            quiverUpdated?.Invoke();
        }

        public JToken CaptureAsJToken()
        {
            JObject state = new JObject
            {
                [nameof(currentAmmunition)] = currentAmmunition != null ? currentAmmunition.GetItemID() : "",
                [nameof(amount)] = amount
            };
            return state;
        }

        public void RestoreFromJToken(JToken state)
        {
            currentAmmunition = null;
            amount = 0;
            if (state is JObject stateDict)
            {
                if (stateDict.TryGetValue(nameof(currentAmmunition), out JToken token))
                {
                    var item = InventoryItem.GetFromID(token.ToString());
                    if (item is AmmunitionItem ammunitionItem)
                    {
                        currentAmmunition = ammunitionItem;
                        if (stateDict.TryGetValue(nameof(amount), out JToken amountToken) &&
                            int.TryParse(amountToken.ToString(), out int number))
                        {
                            amount = number;
                        }
                    }
                }
            }
            quiverUpdated?.Invoke();
        }

        public int SavePriority()
        {
            return 1;
        }
    }
}