using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using RPG.Stats;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Inventories
{
    public class RandomDropper : ItemDropper
    {
        // CONFIG DATA
        [Tooltip("How far can the pickups be scattered from the dropper.")]
        [SerializeField] float scatterDistance = 1;
        [SerializeField] DropLibrary dropLibrary;

        // CONSTANTS
        const int ATTEMPTS = 30;

        public void RandomDrop()
        {
            var baseStats = GetComponent<BaseStats>();

            var drops = dropLibrary.GetRandomDrops(baseStats.GetLevel());
            foreach (var drop in drops)
            {
                DropItem(drop.item, drop.number);
            }
        }

        private int numberToKeep = 5;
        public void DropInventoryItems()
        {
            Inventory inventory = Inventory.GetPlayerInventory();
            int keep = 0;
            foreach (Inventory.InventorySlot inventorySlot in inventory.GetAllInventoryItems().ToArray())
            {
                if (keep < numberToKeep) continue;
                keep++;
                inventory.RemoveItem(inventorySlot.item, inventorySlot.number);
                DropItem(inventorySlot.item, inventorySlot.number);
            }
        }

        public void DropIDroppableItems()
        {
            int keep = -1;
            foreach (DroppableItem droppableItem in GetAllDroppableItems().OrderByDescending(d=>d.number*d.relativeValue))
            {
                keep++;
                if (keep < numberToKeep) continue;
                droppableItem.dropItemCallback?.Invoke(); //Remove the items
                DropItem(droppableItem.item, droppableItem.number);
            }
            TriggerDroppableUpdates();
        }

        void TriggerDroppableUpdates()
        {
            foreach (IDroppableItemHolder holder in GetComponents<IDroppableItemHolder>()) 
            {
                holder.TriggerUpdated();
            }
        }
        
        IEnumerable<DroppableItem> GetAllDroppableItems()
        {
            foreach (IDroppableItemHolder itemHolder in GetComponents<IDroppableItemHolder>())
            {
                foreach (DroppableItem droppableItem in itemHolder.GetDroppableItems())
                {
                    yield return droppableItem;
                }
            }
        }
        
        protected override Vector3 GetDropLocation()
        {
            // We might need to try more than once to get on the NavMesh
            for (int i = 0; i < ATTEMPTS; i++)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * scatterDistance;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 0.1f, NavMesh.AllAreas))
                {
                    return hit.position;
                }
            }
            return transform.position;
        }
    }
}