using GameDevTV.UI;
using RPG.Core;
using RPG.Movement;
using UnityEngine;

namespace RPG.Inventories
{
    public class BankAction : MonoBehaviour, IAction
    {
        private Transform target;
        private System.Action callback;

        public void Cancel()
        {
            ShowHideUI.CloseAllWindows();
            target = null;
        }
        

        public void StartBankAction(Transform bankTarget, System.Action callback)
        {
            if (bankTarget == null) return;
            this.callback = callback;
            target = bankTarget;
            GetComponent<ActionScheduler>().StartAction(this);
        }

        private void Update()
        {
            if (target == null) return;
            if (Vector3.Distance(transform.position, target.position) > 3)
            {
                GetComponent<Mover>().MoveTo(target.position, 1);
            }
            else
            {
                target = null;
                GetComponent<Mover>().Cancel();
                callback?.Invoke();
            }
        }
    }
}