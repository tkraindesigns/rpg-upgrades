using GameDevTV.Inventories;
using UnityEngine;

namespace RPG.Inventories
{
    public class EquipmentLoader : MonoBehaviour
    {
        [SerializeField] private EquipableItem[] itemsToLoad;

        void Awake()
        {
            if (TryGetComponent(out Equipment equipment))
            {
                foreach (EquipableItem item in itemsToLoad)
                {
                    equipment.AddItem(item.GetAllowedEquipLocation(), item);
                }
            }
        }
    }
}