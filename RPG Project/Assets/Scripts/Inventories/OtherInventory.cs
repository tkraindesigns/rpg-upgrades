﻿using System;
using System.Linq;
using GameDevTV.UI;
using RPG.Control;
using RPG.Inventories;
using UnityEngine;

namespace GameDevTV.Inventories
{
    [RequireComponent(typeof(Inventory))]
    public class OtherInventory : MonoBehaviour, IRaycastable
    {
        private ShowHideUI showHideUI;
        
        private void Awake()
        {
            showHideUI = FindObjectsOfType<ShowHideUI>().FirstOrDefault(s => s.HasOtherInventory);
        }

        public CursorType GetCursorType()
        {
            return CursorType.Pickup;
        }

        public bool HandleRaycast(PlayerController callingController)
        {
            if (showHideUI == null) return false;
            if (Input.GetMouseButtonDown(0))
            {
                 callingController.GetComponent<BankAction>().StartBankAction(transform, () =>
                 {
                     showHideUI.ShowOtherInventory(gameObject);
                 });
            }
            return true;
        }
    }
}