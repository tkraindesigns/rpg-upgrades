﻿using GameDevTV.Inventories;
using RPG.Core;
using RPG.Movement;
using UnityEngine;

namespace RPG.Inventories
{
    public class ItemCollector:MonoBehaviour, IAction
    {
        private Pickup pickup;


        private void Update()
        {
            if (pickup == null) return;
            if (Vector3.Distance(transform.position, pickup.transform.position) < 3.0f)
            {
                GetComponent<Mover>().MoveTo(pickup.transform.position, 1f);
            }
            else
            {
                GetComponent<Mover>().Cancel();
                pickup.PickupItem();
                pickup = null;
            }
        }

        public void CollectItem(Pickup pickup)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            this.pickup = pickup;
        }
        
        public void Cancel()
        {
            pickup = null;
        }
    }
}