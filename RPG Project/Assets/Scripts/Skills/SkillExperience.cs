﻿using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace RPG.Skills
{
    public enum Skill
    {
        Melee,
        Casting,
        Ranged,
        Dodging,
        Resisting,
        Skinning,
        Herbalism,
        LumberJacking,
        Mining,
        Smelting,
        Blacksmithing,
        LeatherWorking,
        PotionMaking,
        Fishing,
        Defense,
        Magic
    }
    
    /// <summary>
    /// Convenient container for all things skill related.  What value represents
    /// depends on context, and could represent experience, a level, or a base value per level. 
    /// </summary>
    [System.Serializable]
    public struct SkillRecord
    {
        public Skill skill; 
        public int value;
    }
    
    public class SkillExperience : MonoBehaviour, IJsonSaveable
    {
        private Dictionary<Skill, int> experiences = new();

        public event System.Action skillExperienceUpdated;

        /// <summary>
        /// Awards experience points towards a specific skill.
        /// This will trigger skillExperienceUpdated event
        /// </summary>
        /// <param name="skill">Skill to increase</param>
        /// <param name="experience">Amount to award</param>
        public void GainExperience(Skill skill, int experience)
        {
            if(!experiences.TryAdd(skill, experience))
            {
                experiences[skill] += experience;
            }
            skillExperienceUpdated?.Invoke();
        }

        /// <summary>
        /// Returns current experience points of a given skill.  
        /// </summary>
        /// <param name="skill">Skill to retrieve</param>
        /// <returns>Current experience.  0 if the skill has not been added to the experiences.</returns>
        public int GetExperience(Skill skill)
        {
            return experiences.TryGetValue(skill, out int result) ? result : 0;
        }

        /// <summary>
        /// Gets a list of all experience values in experience as SkillRecords.   
        /// </summary>
        /// <returns>Collection of SkillRecords where value = experience</returns>
        public IEnumerable<SkillRecord> GetSkillRecords()
        {
            foreach (KeyValuePair<Skill,int> pair in experiences)
            {
                yield return new SkillRecord() { skill = pair.Key, value = pair.Value };
            }
        }
        
        
        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(experiences);
        }

        public void RestoreFromJToken(JToken state)
        {
            experiences = state.ToObject<Dictionary<Skill, int>>();
            skillExperienceUpdated?.Invoke();
        }

        public int SavePriority()
        {
            return 0;
        }
    }
}