using System.Collections.Generic;
using UnityEngine;

namespace RPG.Skills
{
    /// <summary>
    /// ScriptableObject to hold base costs for given skills.  This class holds only the base cost, which is
    /// then used in the SkillStore to calculate a given value per level using level! * cost.
    /// </summary>
    [CreateAssetMenu(fileName = "SkillCosts", menuName = "Skills/SkillFormula")]
    public class SkillFormula : ScriptableObject
    {
        [Header("Default")]
        [SerializeField] 
        [Tooltip("Base cost for any skill not in specific skill Base Costs")]private int defaultBaseCost = 10;
        [Header("Overrides")]
        [Tooltip("Enter any specific skills with custom base costs here")]
        [SerializeField] private List<SkillRecord> specificSkillBaseCosts = new List<SkillRecord>();
        
        /// <summary>
        /// Do not use this, it is the backing field for baseCosts
        /// </summary>
        private Dictionary<Skill, int> _baseCosts;

        /// <summary>
        /// Dictionary with cached values for customized base costs.  Uses lazy initialization.
        /// </summary>
        Dictionary<Skill, int> baseCosts
        {
            get
            {
                if (_baseCosts == null)
                {
                    _baseCosts = new Dictionary<Skill, int>();
                    foreach (SkillRecord record in specificSkillBaseCosts)
                    {
                        _baseCosts[record.skill] = record.value;
                    }
                }
                return _baseCosts;
            }
        }
        /// <summary>
        /// This is a little syntatic sugar trick.  Rather than using a public GetValue() method, this overrides
        /// the [] indexer, allowing you to get a specific record using [].  Indexer is safe for both when there is and
        /// isn't an entry in the skill, the default base cost is returned.
        /// </summary>
        /// <param name="skill">Skill to retrieve the base cost.</param>
        public int this[Skill skill] => baseCosts.TryGetValue(skill, out int result) ? result : defaultBaseCost;
    }
}