using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace RPG.Skills
{
    public class SkillStore : MonoBehaviour, IJsonSaveable
    {
        [Header("Starting Skills")]
        [Tooltip("Enter any starting skills here.  Any skill not in this list will start at level 1")]
        [SerializeField] private List<SkillRecord> startingSkills;
        [Header("Base Requirements")]
        [Tooltip("If there is no formula attached, defaultBaseRequirement is used.")]
        [SerializeField] private int defaultBaseRequirement=10;
        [Tooltip("Attach a SkillFormula here denoting base requirements for each skill (or a default value for ones not listed")]
        [SerializeField] private SkillFormula formulas;
        
        public event System.Action skillsUpdated;
        private SkillExperience skillExperience;
        
        //Backing field, do not use!
        private Dictionary<Skill, int> _skills;
        
        /// <summary>
        /// Use this skills Dictionary, not the backing field.  Dictionary of all skills with levels set.  
        /// </summary>
        Dictionary<Skill, int> skills
        {
            get
            {
                if (_skills==null)
                {
                    _skills = new Dictionary<Skill, int>();
                    foreach (SkillRecord skill in startingSkills)
                    {
                        _skills.Add(skill.skill, skill.value);
                    }
                }
                return _skills;
            }
        }

        
        
        void Awake()
        {
            if (TryGetComponent(out skillExperience))
            {
                skillExperience.skillExperienceUpdated += CheckExperiences;
            }

            for (int i = 1; i < 201; i++)
            {
                Debug.Log($"Cost at level {i} = {ExperienceNeeded(Skill.Blacksmithing)}");
                GainLevel(Skill.Blacksmithing);
            }
        }

        /// <summary>
        /// Event handler.  Since this method can only be called by an event on an attached skillExperience
        /// component, it can be safely assumed that skillExperience is not null.
        /// Goes through each skill within the SkillExperience component, checking to see if the exerience
        /// is high enough to level any given skill.
        /// </summary>
        private void CheckExperiences()
        {
            foreach (SkillRecord skillRecord in skillExperience.GetSkillRecords())
            {
                int experience = skillRecord.value;
                while (experience > ExperienceNeeded(skillRecord.skill))
                {
                    GainLevel(skillRecord.skill);
                }
                
            }
        }

        /// <summary>
        /// Increases the passed in skill level by 1.  Should only be called by CheckExperiences.
        /// </summary>
        /// <param name="skill">Skill to level up</param>
        private void GainLevel(Skill skill)
        {
            skills[skill] = GetSkillLevel(skill) + 1;
            skillsUpdated?.Invoke();
        }

        
        
        /// <summary>
        /// This method will calculate a skill's required level based on a set base requirement.  The Base requirement is
        /// what the skill will cost to advance from level 1 to level 2.  Thereafter, the formula multiplies the level and adds it to
        /// the base requirement.  This is functionally an n factorial method, but with a multiplier for the final result. 
        /// </summary>
        /// <param name="skill">Skill to check</param>
        /// <returns>Amount of experience needed for this level.</returns>
        private int ExperienceNeeded(Skill skill)
        {
            skills.TryAdd(skill, 1);
            int level = skills[skill];
            int baseRequirement = GetBaseRequirement(skill);
            int result = 0;
            for (int i = 1; i <= level; i++)
            {
                result += i * baseRequirement;
            }
            return result;
        }

        /// <summary>
        /// Gets the current level for the passed skill. All level start at 1 unless otherwise specified in the
        /// starting Skills list.
        /// </summary>
        /// <param name="skill">Skill to locate</param>
        /// <returns>Current level of that skill</returns>
        public int GetSkillLevel(Skill skill)
        {
            return skills.TryGetValue(skill, out int result) ? result : 1;
        }

        /// <summary>
        /// Uses an attached SkillFormula to derive a base value for the given skill.  If no SkillFormula is attached,
        /// then all skills will use the value in defaultBaseRequirement.
        /// </summary>
        /// <param name="skill">Skill to get base requirement</param>
        /// <returns>Value used to seed ExerienceNeeded</returns>
        int GetBaseRequirement(Skill skill)
        {
            if (formulas) return formulas[skill];
            return defaultBaseRequirement;
        }
        
        
        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(_skills);
        }

        public void RestoreFromJToken(JToken state)
        {
            _skills = state.ToObject<Dictionary<Skill, int>>();
            skillsUpdated?.Invoke();
        }

        public int SavePriority()
        {
            return -1;
        }

        private int GetCombatLevelBasedOnLowestSkill()
        {
            int level = int.MaxValue;
            level = Mathf.Min(level, GetSkillLevel(Skill.Melee));
            level = Mathf.Min(level, GetSkillLevel(Skill.Defense));
            level = Mathf.Min(level, GetSkillLevel(Skill.Ranged));
            level = Mathf.Min(level, GetSkillLevel(Skill.Magic));
            return level;
        }

        private int GetCombatLevelBasedOnRuleOfFive()
        {
            return (GetSkillLevel(Skill.Melee) + GetSkillLevel(Skill.Defense) + GetSkillLevel(Skill.Ranged) + GetSkillLevel(Skill.Magic)) / 5;
        }

        public int GetCombatLevel()
        {
            return Mathf.Max(GetCombatLevelBasedOnLowestSkill(), GetCombatLevelBasedOnRuleOfFive());
        }
    }
}