﻿using System.Collections;
using System.Collections.Generic;
using RPG.Combat;
using RPG.Control;
using RPG.Stats;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;


namespace RPG.Respawnables
{
    public class RespawnManager : MonoBehaviour
    {
        [SerializeField] private GameObject mobPrefab;
        [SerializeField] private List<GameObject> mobPrefabs = new List<GameObject>();
        [SerializeField] private GameObject awakenParticlePrefab;
        [SerializeField] private float delay = 2.0f;
        [SerializeField] private AggroGroup aggroGroup;
        [SerializeField] private PatrolPath patrolPath;
        
        private GameObject spawnedEnemy;
        private bool isRespawning=true;
        public UnityEvent onRespawn;
        

        



        private void OnValidate()
        {
            if (mobPrefabs.Count == 0 && mobPrefab != null)
            {
                mobPrefabs.Add(mobPrefab);
            }
        }

        private void Awake()
        {
            var tr = transform;
            //Clear any existing children.  This allows you to put a place holder mob on the generator.
            foreach (Transform child in tr) Destroy(child.gameObject);
        }

        void Start()
        {
            isRespawning = true;
            Respawn();
        }


        private void Update()
        {
            if (spawnedEnemy || isRespawning) return;
           
            isRespawning = true;
            Invoke(nameof(Respawn), delay);
        }

        public void Respawn()
        {
            if (mobPrefabs.Count > 0)
            {
                mobPrefab = mobPrefabs[Random.Range(0, mobPrefabs.Count)];
            }

            StartCoroutine(RespawnCoroutine());
        }

        IEnumerator RespawnCoroutine()
        {
            yield return new WaitForSeconds(Random.Range(.1f, 1f));
            ClearTransformAndSpawnMob();
            SpawnParticle();
            isRespawning = false;
        }
        private void SpawnParticle()
        {
            if (awakenParticlePrefab)
            {
                GameObject awakenParticle =
                    Instantiate(this.awakenParticlePrefab, transform.position, Quaternion.identity);
                Destroy(awakenParticle.gameObject, 2);
            }
        }

        private void ClearTransformAndSpawnMob()
        {
            var tr = transform;
            //Clear any existing children.  This allows you to put a place holder mob on the generator.
            foreach (Transform child in tr) Destroy(child.gameObject);


            spawnedEnemy = Instantiate(mobPrefab, tr.position, tr.rotation);
            spawnedEnemy.transform.SetParent(transform);
            if (aggroGroup != null)
            {
                aggroGroup.AddFighterToGroup(spawnedEnemy.GetComponent<Fighter>());
                if (spawnedEnemy.TryGetComponent(out DialogueAggro dialogueAggro))
                {
                    dialogueAggro.SetAggroGroup(aggroGroup);
                }
            }

            if (patrolPath != null)
            {
                spawnedEnemy.GetComponent<AIController>().SetPatrolPath(patrolPath);
            }
        }

        int WeightedRandom(int min, int max)
        {
            List<int> chances = new List<int>();
            int rolling = 0;
            for (int i = max; i >= min; i--, rolling++)
            for (int j = 0; j < rolling; j++)
                chances.Add(i);

            return chances[Random.Range(0, chances.Count)];
        }
    }
}