using UnityEngine;
using RPG.Combat;
 
namespace RPG.Respawnables {
//This class will only go on the character PREFAB that has a dialogue.
    public class DialogueAggro : MonoBehaviour
    {
        //As this is going to be on a prefab, you cannot serialize a scene reference in a prefab...
        //[SerializeField] AggroGroup aggroGroup; 
        AggroGroup aggroGroup;
 
        //This is the method that your DialogueTrigger will call to Aggrevate. 
        public void CallAggroGroup() 
        { 
            if(aggroGroup!=null) aggroGroup.Activate(true); 
        }
 
        //Sets the aggrogroup, called by RespawnManager so that when the trigger fires, this
        //class has a link to the AggroGroup.
        public void SetAggroGroup(AggroGroup aggroGroup2) 
        {
            aggroGroup = aggroGroup2;
        }

    } 
}