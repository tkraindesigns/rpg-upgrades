﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Dialogue
{
    public class DialoguePassthroughTriggerReceiver : MonoBehaviour
    {
        [SerializeField] private DialoguePassthrough trigger;

        public UnityEvent OnTrigger;
        void OnEnable()
        {
            if (trigger != null)
            {
                trigger.TriggerActivated += Trigger;
            }
        }

        private void OnDisable()
        {
            if (trigger != null)
            {
                trigger.TriggerActivated -= Trigger;
            }
        }

        private void Trigger()
        {
            OnTrigger?.Invoke();
        }
    }
}