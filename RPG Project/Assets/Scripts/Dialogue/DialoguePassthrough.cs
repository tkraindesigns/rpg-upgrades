﻿using RPG.Core;
using UnityEngine;

namespace RPG.Dialogue
{
    public class DialoguePassthrough : ScriptableObject, ITriggerable
    {
        public event System.Action TriggerActivated;
        
        public void Trigger()
        {
            TriggerActivated?.Invoke();
        }
    }
}

namespace RPG.Core
{
    public interface ITriggerable
    {
        public void Trigger();
    }
}