using System;
using RPG.Core;
using RPG.Movement;
using UnityEngine;

namespace RPG.Shops 
{
    public class Shopper : MonoBehaviour, IAction
    {
        Shop activeShop = null;

        public event Action activeShopChange;

        private Shop target;

        void Update()
        {
            if (!target) return;
            if (Vector3.Distance(transform.position, target.transform.position) > 3) //Make this a variable?
            {
                GetComponent<Mover>().MoveTo(target.transform.position, 1);
            }
            else
            {
                SetActiveShop(target);
                target = null;
                GetComponent<Mover>().Cancel();
            }
        }
        
        public void StartShopAction(Shop targetShop)
        {
            if (targetShop == null)
            {
                Cancel();
                return;
            }
            GetComponent<ActionScheduler>().StartAction(this);
            this.target = targetShop;
        }
        
        public void SetActiveShop(Shop shop)
        {
            if (activeShop != null)
            {
                activeShop.SetShopper(null);
            }
            activeShop = shop;
            if (activeShop != null)
            {
                
                activeShop.SetShopper(this);
            }
            if (activeShopChange != null)
            {
                activeShopChange();
            }
        }

        public void Cancel()
        {
            SetActiveShop(null);
        }
        
        public Shop GetActiveShop()
        {
            return activeShop;
        }
    }
}