﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] Health healthComponent = null;
        [SerializeField] RectTransform foreground = null;
        [SerializeField] Canvas rootCanvas = null;

        private void Awake()
        {
            healthComponent.takeDamage.AddListener(UpdateDisplay);
        }

        void UpdateDisplay(float amount)
        {
            StopAllCoroutines();
            if (Mathf.Approximately(healthComponent.GetFraction(), 0)
            ||  Mathf.Approximately(healthComponent.GetFraction(), 1))
            {
                rootCanvas.enabled = false;
                return;
            }
            
            rootCanvas.enabled = true;
            foreground.localScale = new Vector3(healthComponent.GetFraction(), 1, 1);
            StartCoroutine(HideCanvasAfterFiveSeconds());
        }

        IEnumerator HideCanvasAfterFiveSeconds()
        {
            yield return new WaitForSeconds(5);
            rootCanvas.enabled = false;
        }
    }
}