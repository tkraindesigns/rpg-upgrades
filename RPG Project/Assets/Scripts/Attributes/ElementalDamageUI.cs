﻿using GameDevTV.Inventories;
using RPG.Stats;
using TMPro;
using UnityEngine;

namespace RPG.Attributes
{
    public class ElementalDamageUI : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI text;
        [SerializeField] DamageType damageType;
     
        GameObject player;

        void Awake()
        {
            player = GameObject.FindWithTag("Player");
            // This event is likely mispelled, I don't have the code on me
            player.GetComponent<Equipment>().equipmentUpdated+=UpdateUI; 
            player.GetComponent<BaseStats>().onLevelUp+=UpdateUI;
        }
        void Start()
        {
            UpdateUI();
        }
        void UpdateUI()
        {
            float damage = 0;
            foreach(var provider in player.GetComponents<IElementalDamageProvider>())
            foreach(var amount in provider.GetElementalDamageBoost(damageType))
                damage+=amount;
            text.text = $"{damage}";
        }
    }
}