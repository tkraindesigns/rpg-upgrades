﻿using System;
using GameDevTV.UI;
using UnityEngine;

namespace DefaultNamespace
{
    public class SuppressOnModal : MonoBehaviour
    {
        private void OnEnable()
        {
            if(ShowHideUI.IsModalActive) gameObject.SetActive(false);
        }
    }
}