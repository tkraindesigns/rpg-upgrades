﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RPG.Stats.Edit
{
    [CustomEditor(typeof(Progression))]
    public class ProgressionEditor : Editor
    {
        private SerializedProperty characterClasses;
        private Dictionary<int, bool> expanded = new Dictionary<int, bool>();

        void OnEnable()
        {
            characterClasses = serializedObject.FindProperty(nameof(characterClasses));
        }
        public override void OnInspectorGUI()
        {
            GUIStyle style = new GUIStyle
            {
                margin = new RectOffset(25, 0, 0, 0)
            };
            int elementCount = characterClasses.arraySize;
            int elementToDelete = -1;
            for (int i = 0; i < elementCount; i++)
            {
               
                SerializedProperty progressionClass = characterClasses.GetArrayElementAtIndex(i);
                SerializedProperty characterClass = progressionClass.FindPropertyRelative(nameof(characterClass));
                SerializedProperty stats = progressionClass.FindPropertyRelative(nameof(stats));
                int enumValue = characterClass.enumValueIndex;
                string className = ((CharacterClass)enumValue).ToString();
                bool foldout = false;
                if(expanded.TryGetValue(i, out foldout)){}
                expanded[i] = foldout = EditorGUILayout.Foldout(foldout, className);
                if(foldout)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(characterClass,
                        new GUIContent(className));
                    if (GUILayout.Button("Delete")) elementToDelete = i;
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginVertical(style);
                    int statToDelete = -1;
                    for (int j = 0; j < stats.arraySize; j++)
                    {
                        SerializedProperty entry = stats.GetArrayElementAtIndex(j);
                        SerializedProperty stat = entry.FindPropertyRelative(nameof(stat));
                        SerializedProperty levels = entry.FindPropertyRelative(nameof(levels));
                        EditorGUILayout.BeginHorizontal();
                        string statName = stat.enumDisplayNames[stat.enumValueIndex];
                        EditorGUILayout.PropertyField(stat, new GUIContent(statName));
                        if (GUILayout.Button($"Delete")) statToDelete = j;
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginVertical(style);
                        EditorGUILayout.PropertyField(levels);
                        EditorGUILayout.EndVertical();
                    }
                    if (GUILayout.Button("Add Stat"))
                    {
                        stats.InsertArrayElementAtIndex(stats.arraySize);   
                    }
                    if (statToDelete >= 0)
                    {
                        stats.DeleteArrayElementAtIndex(statToDelete);
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            if (GUILayout.Button("Add Class"))
            {
                characterClasses.InsertArrayElementAtIndex(characterClasses.arraySize);                
            }
            if (elementToDelete >= 0)
            {
                characterClasses.DeleteArrayElementAtIndex(elementToDelete);                
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}