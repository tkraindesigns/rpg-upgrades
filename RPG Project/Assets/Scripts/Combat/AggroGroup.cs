﻿using System;
using System.Collections.Generic;
using RPG.Attributes;
using UnityEngine;

namespace RPG.Combat
{
    public class AggroGroup : MonoBehaviour
    {
        //[SerializeField] Fighter[] fighters; 
        //Making this a list so that it can be easily added to or subtracted from
        [SerializeField] private List<Fighter> fighters = new List<Fighter>();
        [SerializeField] bool activateOnStart = false;
        private bool isActivated;

        private void Awake()
        {
            Health playerHealth = GameObject.FindWithTag("Player").GetComponent<Health>();
            if (playerHealth)
            {
                playerHealth.onDie.AddListener(()=>Activate(activateOnStart));
            }
        }

        private void Start() {
            Activate(activateOnStart);
        }

        public void Activate(bool shouldActivate)
        {
            isActivated = shouldActivate;
            foreach (Fighter fighter in fighters)
            {
                if (!fighter)
                {
                    Debug.Log($"Invalid fighter in configuration");
                    continue;
                }
                fighter.enabled = shouldActivate;
                if (fighter.TryGetComponent(out CombatTarget target))
                {
                    target.enabled = shouldActivate;
                }
            }
        }

        public void AddFighterToGroup(Fighter fighter)
        {
            if (fighters.Contains(fighter)) return;
            fighters.Add(fighter);
            fighter.enabled = isActivated;
            if (fighter.TryGetComponent(out CombatTarget target))
            {
                target.enabled = isActivated;
            }
        }

        public void RemoveFighterFromGroup(Fighter fighter)
        {
            fighters.Remove(fighter); //Because parameter to Remove can be null, no need to null check.
        }
        
    }
}