﻿using System.Collections.Generic;

namespace GameDevTV.Inventories
{
    public struct DroppableItem
    {
        public InventoryItem item;
        public int number;
        public float relativeValue;
        public System.Action dropItemCallback;
    }
    
    public interface IDroppableItemHolder
    {
        IEnumerable<DroppableItem> GetDroppableItems();
        void TriggerUpdated();
    }
}