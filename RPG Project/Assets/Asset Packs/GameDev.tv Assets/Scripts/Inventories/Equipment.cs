﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;
using GameDevTV.Utils;
using Newtonsoft.Json.Linq;
using UnityEngine.TextCore.LowLevel;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// Provides a store for the items equipped to a player. Items are stored by
    /// their equip locations.
    /// 
    /// This component should be placed on the GameObject tagged "Player".
    /// </summary>
    public class Equipment : MonoBehaviour, IJsonSaveable, IPredicateEvaluator, IDroppableItemHolder
    {
        // STATE
        Dictionary<EquipLocation, EquipableItem> equippedItems = new Dictionary<EquipLocation, EquipableItem>();

        // PUBLIC

        /// <summary>
        /// Broadcasts when the items in the slots are added/removed.
        /// </summary>
        public event Action equipmentUpdated;

        /// <summary>
        /// Return the item in the given equip location.
        /// </summary>
        public EquipableItem GetItemInSlot(EquipLocation equipLocation)
        {
            if (!equippedItems.ContainsKey(equipLocation))
            {
                return null;
            }

            return equippedItems[equipLocation];
        }

        /// <summary>
        /// Add an item to the given equip location. Do not attempt to equip to
        /// an incompatible slot.
        /// </summary>
        public void AddItem(EquipLocation slot, EquipableItem item)
        {
            //Debug.Assert(item.CanEquip(slot, this));
            if (!IsValidEquipLocation(slot, item)) return;
            equippedItems[slot] = item;

            if (equipmentUpdated != null)
            {
                equipmentUpdated();
            }
        }

        /// <summary>
        /// Remove the item for the given slot.
        /// </summary>
        public void RemoveItem(EquipLocation slot)
        {
            if (equippedItems.TryGetValue(slot, out EquipableItem item) && item.UnRemovable()) return;
            equippedItems.Remove(slot);
            if (equipmentUpdated != null)
            {
                equipmentUpdated();
            }
        }

        /// <summary>
        /// Enumerate through all the slots that currently contain items.
        /// </summary>
        public IEnumerable<EquipLocation> GetAllPopulatedSlots()
        {
            return equippedItems.Keys;
        }

        // PRIVATE

        public bool IsValidEquipLocation(EquipLocation location, EquipableItem item)
        {
            //Always return true if the location == the equiplocation
            if (item.GetAllowedEquipLocation() == location) return true;
            if (location == EquipLocation.Ring2) return item.GetAllowedEquipLocation() == EquipLocation.Ring;
            //Add additional corner cases here
            return false;
        }

        public bool? Evaluate(EPredicate predicate, string[] parameters)
        {
            if (predicate == EPredicate.HasItemEquipped)
            {
                foreach (var item in equippedItems.Values)
                {
                    if (item.GetItemID() == parameters[0])
                    {
                        return true;
                    }
                }
                return false;
            }
            return null;
        }

        public JToken CaptureAsJToken()
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            foreach (var pair in equippedItems)
            {
                stateDict[pair.Key.ToString()] = JToken.FromObject(pair.Value.GetItemID());
            }
            return state;
        }

        public void RestoreFromJToken(JToken state)
        {
            if(state is JObject stateObject)
            {
                equippedItems.Clear();
                IDictionary<string, JToken> stateDict = stateObject;
                foreach (var pair in stateObject)
                {
                    if (Enum.TryParse(pair.Key, true, out EquipLocation key))
                    {
                        if (InventoryItem.GetFromID(pair.Value.ToObject<string>()) is EquipableItem item)
                        {
                            equippedItems[key] = item;
                        }
                    }
                }
            }
            equipmentUpdated?.Invoke();
        }

        public int SavePriority()
        {
            return 8;
        }

        public IEnumerable<DroppableItem> GetDroppableItems()
        {
            foreach (KeyValuePair<EquipLocation, EquipableItem> pair in equippedItems)
            {
                if (pair.Value == null) continue;
                EquipLocation equipLocation = pair.Key;
                DroppableItem droppableItem = new DroppableItem()
                {
                    item = pair.Value,
                    number = 1,
                    relativeValue = pair.Value.GetPrice(),
                    dropItemCallback = () =>
                    {
                        equippedItems.Remove(equipLocation);
                    }
                };
                yield return droppableItem;
            }
        }

        public void TriggerUpdated()
        {
            equipmentUpdated?.Invoke();
        }
    }
}