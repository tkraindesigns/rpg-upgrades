using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameDevTV.Inventories.Editors
{
    //When we start subclassing this editor, remove the editorForChildClasses:true.  Needed here only for testing.
    [CustomEditor(typeof(InventoryItem), editorForChildClasses:true)] 
    public class InventoryItemEditor : Editor
    {
 
        // For reference
        // [Tooltip("Auto-generated UUID for saving/loading. Clear this field if you want to generate a new one.")]
        // [SerializeField] string itemID = null;
        // [Tooltip("Item name to be displayed in UI.")]
        // [SerializeField] string displayName = null;
        // [Tooltip("Item description to be displayed in UI.")]
        // [SerializeField][TextArea] string description = null;
        // [Tooltip("The UI icon to represent this item in the inventory.")]
        // [SerializeField] Sprite icon = null;
        // [Tooltip("The prefab that should be spawned when this item is dropped.")]
        // [SerializeField] Pickup pickup = null;
        // [Tooltip("If true, multiple items of this type can be stacked in the same inventory slot.")]
        // [SerializeField] bool stackable = false;
        // [SerializeField] float price;
        // [SerializeField] ItemCategory category = ItemCategory.None;
        
        //It is extremely important that the names of these SerializedProperties match the names of the properties
        //In the code.  When in doubt, cut and paste.  This will become more apparent in OnEnable
        
        private SerializedProperty itemID;
        private SerializedProperty displayName;
        private SerializedProperty description;
        private SerializedProperty icon;
        private SerializedProperty pickup;
        private SerializedProperty stackable;
        private SerializedProperty price;
        private SerializedProperty category;

        private InventoryItem targetInventoryItem;

        private static bool showInventoryItemDetails = true; 
        
        SerializedProperty Find(string propertyToFind)
        {
            return serializedObject.FindProperty(propertyToFind);
        }
        
        private void OnEnable()
        {
            itemID = Find(nameof(itemID));
            displayName = Find(nameof(displayName));
            description = Find(nameof(description));
            icon = Find(nameof(icon));
            pickup = Find(nameof(pickup));
            stackable = Find(nameof(stackable));
            price = Find(nameof(price));
            category = Find(nameof(category));
            //Since this is an InventoryItem Editor, this cast will always work.
            targetInventoryItem = serializedObject.targetObject as InventoryItem;
        }

        public override void OnInspectorGUI()
        {
            showInventoryItemDetails = EditorGUILayout.Foldout(showInventoryItemDetails, "Basic Properties");
            if (!showInventoryItemDetails) return;
            DrawItemID();
            EditorGUILayout.PropertyField(displayName);
            EditorGUILayout.PropertyField(description);
            EditorGUILayout.PropertyField(icon);
            EditorGUILayout.PropertyField(pickup);
            EditorGUILayout.PropertyField(stackable);
            EditorGUILayout.PropertyField(price);
            EditorGUILayout.PropertyField(category);
        }

        private void DrawItemID()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField($"ItemID: {itemID.stringValue}");
            if (GUILayout.Button("Change") || string.IsNullOrEmpty(itemID.stringValue))
            {
                itemID.stringValue = System.Guid.NewGuid().ToString();
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}