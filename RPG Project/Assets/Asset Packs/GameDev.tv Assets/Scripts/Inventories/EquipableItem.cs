using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Utils;
using RPG.Stats;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// An inventory item that can be equipped to the player. Weapons could be a
    /// subclass of this.
    /// </summary>
    [CreateAssetMenu(menuName = ("GameDevTV/GameDevTV.UI.InventorySystem/Equipable Item"))]
    public class EquipableItem : InventoryItem
    {
        // CONFIG DATA
        [Tooltip("Where are we allowed to put this item.")]
        [SerializeField] EquipLocation allowedEquipLocation = EquipLocation.Weapon;
        [SerializeField] Condition equipCondition;
        [SerializeField] private List<CharacterClass> allowedClasses = new List<CharacterClass>();
        [SerializeField] private bool unRemovable;

        public bool UnRemovable() => unRemovable;
        public bool IsEquippableByClass(CharacterClass characterClass)
        {
            if (allowedClasses.Count == 0) return true;
            return allowedClasses.Contains(characterClass);
        }

        // PUBLIC

        public bool CanEquip(EquipLocation equipLocation, Equipment equipment)
        {
            if (!equipment.IsValidEquipLocation(equipLocation, this)) return false;

            return equipCondition.Check(equipment.GetComponents<IPredicateEvaluator>());
        }

        public EquipLocation GetAllowedEquipLocation()
        {
            return allowedEquipLocation;
        }
    }
}