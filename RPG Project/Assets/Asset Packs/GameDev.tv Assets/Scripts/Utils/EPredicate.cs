﻿namespace GameDevTV.Utils
{
    public enum EPredicate
    {
        Select,
        HasQuest,
        CompletedObjective,
        CompletedQuest,
        HasLevel,
        MinimumTrait,
        HasItem,
        HasItems,
        HasItemEquipped,
        HasKilled
    }
    
}