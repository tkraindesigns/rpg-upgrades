﻿using System.Collections.Generic;
using UnityEngine;

namespace GameDevTV.Utils.ResourceRetrieval
{
    // The Where T: ScriptableObject, IHasItemID means only in item that has 
//BOTH of these things qualifies.
    public class ResourceRetriever<T> where T : ScriptableObject, IHasItemID
    {
        //Dictionary already holds all items, no need for separate list.
        static Dictionary<string, T> itemLookupCache;
        
        /// <summary>
        /// Retrieves item represented by itemID.  
        /// </summary>
        /// <param name="itemID">Must be unique across all instances of T</param>
        /// <returns>cached item represented by itemID or null if not in Dictionary</returns>
        public static T GetFromID(string itemID)
        {
            BuildLookup();
            if (itemID == null || !itemLookupCache.ContainsKey(itemID)) return null;
            return itemLookupCache[itemID];
        }

        /// <summary>
        /// All items of the Retriever's type (readonly). 
        /// </summary>
        public static IEnumerable<T> Values
        {
            get
            {
                BuildLookup();
                return itemLookupCache.Values;
            }
        }
        /// <summary>
        /// ItemIDs of all items of the Retriever's type (readonly)
        /// </summary>
        public static IEnumerable<string> Keys
        {
            get
            {
                BuildLookup();
                return itemLookupCache.Keys;
            }
        }
        
        private static void BuildLookup()
        {
            if (itemLookupCache == null)
            {
                itemLookupCache = new Dictionary<string, T>();
                var itemList = Resources.LoadAll<T>("");
                foreach (var item in itemList)
                {
                    if (itemLookupCache.ContainsKey(item.GetItemID()))
                    {
                        Debug.LogError(string.Format("Looks like there's a duplicate  ID for objects: {0} and {1}",
                            itemLookupCache[item.GetItemID()], item));
                        continue;
                    }
                    itemLookupCache[item.GetItemID()] = item;
                }
            }
        }
    }

    public interface IHasItemID
    {
        string GetItemID();
    }
}