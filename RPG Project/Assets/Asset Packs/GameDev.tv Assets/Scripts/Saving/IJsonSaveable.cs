﻿using Newtonsoft.Json.Linq;

namespace GameDevTV.Saving
{
    public interface IJsonSaveable
    {
        JToken CaptureAsJToken();
        void RestoreFromJToken(JToken state);

        int SavePriority();
    }
}