﻿using System;
using System.Linq;
using GameDevTV.UI.Inventories;
using UnityEngine;

namespace GameDevTV.UI
{
    public class ShowHideUI : MonoBehaviour
    {
        [SerializeField] KeyCode toggleKey = KeyCode.SysReq;
        [SerializeField] GameObject uiContainer = null;

        [SerializeField] GameObject otherInventoryContainer=null;
        [SerializeField] InventoryUI otherInventoryUI = null;
        [SerializeField] private bool modal = false;
        [SerializeField] private GameObject craftingTable = null;
        //[SerializeField] CraftingTableUI craftingTableUI;
        private static bool locked = false;
        public bool HasOtherInventory => otherInventoryContainer != null;
        public bool HasCraftingTable => craftingTable != null;
        public static bool IsModalActive => locked;
        public static event System.Action OnModalActive;


        // Start is called before the first frame update
        void Start()
        {
            uiContainer.SetActive(false);
            if(otherInventoryContainer!=null) otherInventoryContainer.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (toggleKey == KeyCode.SysReq) return;
            if (locked && !modal) return;
            if (Input.GetKeyDown(toggleKey))
            {
                Toggle();
            }
        }

        private void OnDestroy()
        {
            if (modal) locked = false;
        }

        public void ShowOtherInventory(GameObject go)
        {
            uiContainer.SetActive(true);
            otherInventoryContainer.SetActive(true);
            otherInventoryUI.Setup(go);
        }

        public void ShowCraftingTable(GameObject go)
        {
            uiContainer.SetActive(true);
            craftingTable.SetActive(true);
            //call setup on crafting table ui
        }

        public void Toggle()
        {
            uiContainer.SetActive(!uiContainer.activeSelf);
            if(otherInventoryContainer!=null) otherInventoryContainer.SetActive(false);
            if (modal)
            {
                if (!locked && uiContainer.activeSelf) CloseOtherWindows();
                else locked = false;
            }
        }

        public void CloseOtherWindows()
        {
            foreach (ShowHideUI ui in FindObjectsOfType<ShowHideUI>().Where(s=>!s.modal))
            {
                ui.uiContainer.SetActive(false);
                if(ui.otherInventoryContainer!=null) ui.otherInventoryContainer.SetActive(false);
            }
            OnModalActive?.Invoke();
            locked = true;
        }

        public static void CloseAllWindows()
        {
            foreach (ShowHideUI hideUI in FindObjectsOfType<ShowHideUI>())
            {
                hideUI.uiContainer.SetActive(false);
                if(hideUI.otherInventoryUI!=null) hideUI.otherInventoryContainer.SetActive(false);
            }
        }
    }
}