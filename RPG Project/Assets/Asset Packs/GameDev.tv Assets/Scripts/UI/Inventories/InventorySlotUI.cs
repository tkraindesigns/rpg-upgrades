﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameDevTV.Inventories;
using GameDevTV.Core.UI.Dragging;
using RPG.Inventories;

namespace GameDevTV.UI.Inventories
{
    public class InventorySlotUI : MonoBehaviour, IItemHolder, IDragContainer<InventoryItem>, IPointerClickHandler
    {
        // CONFIG DATA
        [SerializeField] InventoryItemIcon icon = null;

        // STATE
        int index;
        
        Inventory inventory;

        // PUBLIC

        public void Setup(Inventory inventory, int index)
        {
            this.inventory = inventory;
            this.index = index;
            icon.SetItem(inventory.GetItemInSlot(index), inventory.GetNumberInSlot(index));
        }

        public int MaxAcceptable(InventoryItem item)
        {
            if (inventory.HasSpaceFor(item))
            {
                return int.MaxValue;
            }
            return 0;
        }

        public void AddItems(InventoryItem item, int number)
        {
            inventory.AddItemToSlot(index, item, number);
        }

        public InventoryItem GetItem()
        {
            return inventory.GetItemInSlot(index);
        }

        public int GetNumber()
        {
            return inventory.GetNumberInSlot(index);
        }

        public void RemoveItems(int number)
        {
            inventory.RemoveFromSlot(index, number);
        }

        private static InventorySlotUI LastUIClicked;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            if (LastUIClicked != this)
            {
                LastUIClicked = this;
                Invoke(nameof(TimesUp), .5f);
                Debug.Log($"{index} was clicked once.");
            }
            else
            {
                HandleDoubleClick();
            }
        }
        private void TimesUp()
        {
            if (LastUIClicked == this) LastUIClicked = null;
        }
        
        private void HandleDoubleClick()
        {
            TimesUp();//Prevents triple click from starting another HandleDoubleClick
            InventoryItem item = inventory.GetItemInSlot(index);
            int number = inventory.GetNumberInSlot(index);
            if (item == null || number<1 ) return; //Nothing to do
            if (inventory.gameObject.CompareTag("Player"))
            {
                var otherInventoryUI =
                    FindObjectsOfType<InventoryUI>().FirstOrDefault(ui => ui.IsOtherInventory); //will return the other Inventory or null
                //Check to see if it's not null (should never happen), if it's Active, and if the otherInventory's inventory is valid.
                if (otherInventoryUI != null && otherInventoryUI.gameObject.activeSelf && otherInventoryUI.SelectedInventory!=null)
                {
                    Inventory otherInventory = otherInventoryUI.SelectedInventory;
                    TransferToOtherInventory(otherInventory, item, 1);
                }
                else if(item is EquipableItem equipableItem && inventory.TryGetComponent(out Equipment equipment))
                {
                    EquipItem(equipableItem, equipment, number);
                } else if (item is AmmunitionItem ammunitionItem && inventory.TryGetComponent(out Quiver quiver))
                {
                    TransferToQuiver(ammunitionItem, quiver, number);
                }
            }
            else //if(!inventory.gameObject.CompareTag("Player") means we're the other inventory.
            {
                TransferToOtherInventory(Inventory.GetPlayerInventory(), item, 1);
            }
        }

        private void EquipItem(EquipableItem equipableItem, Equipment equipment, int number)
        {
            if (equipableItem.CanEquip(equipableItem.GetAllowedEquipLocation(), equipment))
            {
                EquipableItem otherEquipableItem = equipment.GetItemInSlot(equipableItem.GetAllowedEquipLocation());
                equipment.RemoveItem(equipableItem.GetAllowedEquipLocation());
                inventory.RemoveFromSlot(index, number);
                equipment.AddItem(equipableItem.GetAllowedEquipLocation(), equipableItem);
                if (otherEquipableItem != null)
                {
                    inventory.AddItemToSlot(index, otherEquipableItem, 1);
                }
                Setup(inventory, index);
            }
        }

        private void TransferToQuiver(AmmunitionItem ammunitionItem, Quiver quiver, int number)
        {
            AmmunitionItem item = quiver.GetItem();
            int amount = quiver.GetAmount();
            inventory.RemoveFromSlot(index, number);
            quiver.AddAmmunition(ammunitionItem, ammunitionItem==item? number+amount : number);
            if (item != null && ammunitionItem!=item)
            {
                inventory.AddItemToSlot(index, item, amount);
            }
        }

        private void TransferToOtherInventory(Inventory otherInventory, InventoryItem item, int number)
        {
            if (otherInventory.HasSpaceFor(item))
            {
                otherInventory.AddToFirstEmptySlot(inventory.GetItemInSlot(index),
                    number);
                inventory.RemoveItem(item, number);
                Setup(inventory, index);
            }
        }

        public void TryHandleRightClick()
        {
            InventoryItem item = inventory.GetItemInSlot(index);
            int number = inventory.GetNumberInSlot(index);
            if (item == null || number<1 ) return; //Nothing to do
            if(!inventory.gameObject.CompareTag("Player"))
            {
                TransferToOtherInventory(Inventory.GetPlayerInventory(), item, number);
                return;
            }
            var otherInventoryUI =
                FindObjectsOfType<InventoryUI>().FirstOrDefault(ui => ui.IsOtherInventory); //will return the other Inventory or null
            //Check to see if it's not null (should never happen), if it's Active, and if the otherInventory's inventory is valid.
            if (otherInventoryUI != null && otherInventoryUI.gameObject.activeSelf && otherInventoryUI.SelectedInventory!=null)
            {
                Inventory otherInventory = otherInventoryUI.SelectedInventory;
                TransferToOtherInventory(otherInventory, item, number);
                return;
            }
            if (item is EquipableItem equipableItem)
            {
                Equipment equipment = inventory.GetComponent<Equipment>();
                EquipableItem equippedItem = equipment.GetItemInSlot(equipableItem.GetAllowedEquipLocation());
                if (!equipableItem.CanEquip(equipableItem.GetAllowedEquipLocation(), equipment)) return;    // The line solely responsible for checking for Predicate conditions, prior to being able to wield a weapon (if you're not high enough of a level, you can't wield that)
                equipment.RemoveItem(equipableItem.GetAllowedEquipLocation());
                equipment.AddItem(equipableItem.GetAllowedEquipLocation(), equipableItem);
                RemoveItems(1);
                
                if (equippedItem != null)
                {
                    AddItems(equippedItem, 1);
                }

            }

            else if (item is ActionItem actionItem)
            {
                ActionStore actionStore = inventory.GetComponent<ActionStore>();
                int slot = actionStore.GetFirstEmptySlot();
                if (slot > -1)
                {
                    actionStore.AddAction(actionItem, slot, GetNumber());
                    RemoveItems(GetNumber());
                }
            }

        }

        
    }
}