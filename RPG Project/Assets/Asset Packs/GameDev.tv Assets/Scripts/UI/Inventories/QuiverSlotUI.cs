﻿using System;
using GameDevTV.Core.UI.Dragging;
using GameDevTV.Inventories;
using RPG.Inventories;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameDevTV.UI.Inventories
{
    public class QuiverSlotUI : MonoBehaviour, IItemHolder, IDragContainer<InventoryItem>, IPointerClickHandler
    {
        [SerializeField] InventoryItemIcon icon = null;
        [SerializeField] private GameObject itemCounter;
        [SerializeField] private TextMeshProUGUI number;
        
        private Quiver quiver;

        private void Awake()
        {
            quiver = Inventory.GetPlayerInventory().GetComponent<Quiver>();
            quiver.quiverUpdated += RedrawUI;
        }

        void RedrawUI()
        {
            icon.SetItem(GetItem());
            itemCounter.SetActive(GetNumber()>0);
            number.text = $"{GetNumber()}";
        }

        public InventoryItem GetItem()
        {
            return quiver.GetItem();
        }

        public int GetNumber()
        {
            return quiver.GetAmount();
        }

        public void RemoveItems(int number)
        {
            quiver.RemoveAmmunition(number);
        }

        public int MaxAcceptable(InventoryItem item)
        {
            return item is AmmunitionItem ammunitionItem ? Int32.MaxValue : 0;
        }

        public void AddItems(InventoryItem item, int number)
        {
            if(item is AmmunitionItem ammunitionItem)
            {
                if (object.ReferenceEquals(ammunitionItem, quiver.GetItem()))
                {
                    quiver.AddAmmunition(ammunitionItem, number + quiver.GetAmount());
                } else
                {
                    quiver.AddAmmunition(ammunitionItem, number);
                }
            }
        }

        private static QuiverSlotUI LastUIClicked;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            if (LastUIClicked != this)
            {
                LastUIClicked = this;
                Invoke(nameof(TimesUp), .5f);
            }
            else
            {
                HandleDoubleClick(quiver.GetComponent<Inventory>());
            }
        }
        private void TimesUp()
        {
            if (LastUIClicked == this) LastUIClicked = null;
        }
        
        public void HandleDoubleClick(Inventory inventory) {

            if (GetItem() is AmmunitionItem ammunitionItem) {

                AmmunitionItem equippedAmmunition = quiver.GetItem();

                if (inventory.HasSpaceFor(equippedAmmunition)) {

                    inventory.AddToFirstEmptySlot(equippedAmmunition, quiver.GetAmount());
                    // THE AMOUNT GOING IN IS PROBABLY THE PROBLEM REGARDING WHY THE RETURN IS JUST 1, NOT ALL THE AMMO THAT WENT IN:
                    quiver.RemoveAmmunition(quiver.GetAmount());

                }

            }

        }
    }
}